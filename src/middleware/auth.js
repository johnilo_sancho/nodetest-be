var jwt = require("jsonwebtoken");

const checkToken = function (req, res, next) {
  try {
    if (req.header("authorization")) {
      var head = req.header("authorization").slice(7);
      const decode = jwt.verify(head, "secret");
      if (decode) {
        console.log("token was verified.");
        req.user = decode;
      }
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({ message: "Unauthorized!" });
  }
};

module.exports = { checkToken };
