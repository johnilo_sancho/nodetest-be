var { test, test1, post, login, signup, read, update, deleted } = require("../controller/test_cont.js");

var testRoute1 = [
  {
    method: "get",
    path: "/function1",
    action: test,
  },
  {
    method: "get",
    path: "/function2",
    action: test1,
  },
  {
    method: "post",
    path: "/post",
    action: post,
  },
  {
    method: "post",
    path: "/login",
    action: login,
  },
  {
    method: "post",
    path: "/signup",
    action: signup,
  },
  {
    method: "get",
    path: "/fetch",
    action: read,
  },
  {
    method: "post",
    path: "/update",
    action: update,
  },
  {
    method: "post",
    path: "/destroy",
    action: deleted,
  },
];

module.exports = {
  testRoute1,
};
