var { email } = require("../controller/emailSender_cont.js");

var emailRoute = [
  {
    method: "get",
    path: "/email",
    action: email,
  },
];
module.exports = {
  emailRoute,
};
