var seq = require("../../configs/db_config.js");
var sequelize = require("sequelize");

const { DataTypes } = sequelize;

const nodeTable = seq.define(
  "node",
  {
    data: {
      type: DataTypes.JSON,
    },
    email: {
      type: DataTypes.STRING(50),
    },
    password: {
      type: DataTypes.STRING(1000),
    },
  },
  {
    freezeTableName: true,
  }
);

module.exports = nodeTable;
