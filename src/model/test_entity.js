var seq = require("../../configs/db_config.js");
var sequelize = require("sequelize");

const { DataTypes } = sequelize;

const testTable = seq.define(
  "test",
  {
    status: {
      type: DataTypes.STRING(50),
    },
  },
  {
    freezeTableName: true,
  }
);

module.exports = testTable;






//status1 → status1 → status2 → statuS2
//status1 → status1 → status2 → statuS2 → StatuS2 → StaTuS2
///status1 → status1 → status2 → statuS2 → StatuS2