var test_e = require("../model/test_entity.js");
var node_e = require("../model/node_entity.js");
var validator = require("validator");
var passval = require("password-validator");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

var test = async (req, res) => {
  //console.log("the function");
  res.send("test 1");
};

var test1 = async (req, res) => {
  //console.log("the function");
  res.send("test 2");
};

var post = async (req, res) => {
  //console.log("the function");
  console.log(req.body);
  res.send(req.body);
};

var login = async (req, res) => {
  var user = await node_e.findOne({ where: { email: req.body.email } });

  if (user) {
    const isMatch = await bcrypt.compare(req.body.password, user.password);
    if (isMatch) {
      let token = jwt.sign({ email: user.email, data: user.data }, "secret", {
        expiresIn: 500,
      });
      res.json({ message: "Login successful!", token: token });
    } else {
      res.status(401).send("Invalid email or password");
    }
  } else {
    res.status(404).send("not found");
  }
};

var signup = async (req, res) => {
  try {
    var schema = new passval();

    schema
      .is()
      .min(8)
      .is()
      .max(100)
      .has()
      .uppercase()
      .has()
      .lowercase()
      .has()
      .digits()
      .has()
      .not()
      .spaces()
      .has()
      .symbols();

    var existing = await node_e.findOne({ where: { email: req.body.email } });

    if (validator.isEmail(req.body.email) && !existing) {
      if (schema.validate(req.body.password)) {
        req.body.password = await bcrypt.hash(req.body.password, 8);
        var created = await node_e.create(req.body);
        let obj = {};
        obj.id = created.id;
        obj.email = created.email;
        obj.data = created.data;

        res.send(obj); // feedback
      } else {
        res.status(422).send("Invalid password");
      }
    } else {
      if (validator.isEmail(req.body.email) && existing) {
        res.status(422).send("Email already taken!");
      }else{
        res.status(422).send("Invalid email");
      }

    }
  } catch (error) {
    console.log(error);
    res.status(500).send("Oops, there is something wrong. " + error);
  }
};

var read = async (req, res) => {
  try {
    var found = await node_e.findOne({ where: { email: req.user.email } });

    res.send(found);
  } catch (error) {
    console.log(error);
    res.status(500).send("Oops, there is something wrong.");
  }
};

var update = async (req, res) => {
  try {
    var found = await test_e.findOne({ where: { id: req.body.id } });

    var updated = await found.update({ status: req.body.status });
    await updated.save();

    res.send(updated);
  } catch (error) {
    console.log(error);
    res.status(500).send("Oops, there is something wrong.");
  }
};

var deleted = async (req, res) => {
  try {
    var deleted = await test_e.destroy({ where: { id: req.body.id } });
    if (deleted) {
      var stat = 200;
      var mes = "Successfully deleted.";
    } else {
      var stat = 404;
      var mes = "Id Not Found!";
    }
    res.status(stat).json({ message: mes });
  } catch (error) {
    console.log(error);
    res.status(500).send("Oops, there is something wrong.");
  }
};

module.exports = {
  test,
  test1,
  post,
  login,
  signup,
  read,
  update,
  deleted,
};
