var nodemailer = require("nodemailer");
var dotenv = require("dotenv");
var {temp1} = require("../utils/emailTemplates/temp1.js")
dotenv.config();

const email = async (req, res) => {
  try {
    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: process.env.EMAIL_USER,
        clientId: process.env.EMAIL_ID,
        clientSecret: process.env.EMAIL_SECRET,
        refreshToken: process.env.EMAIL_REFRESH,
      },
    });
    let mailOptions = {
      from: process.env.EMAIL_USER,
      to: "johnilosancho@gmail.com",
      subject: "Nodemailer Project",
      html: temp1(),
    };

    transporter.sendMail(mailOptions, function (err, data) {
      if (err) {
        console.log("Error " + err);
      } else {
        console.log("Email sent successfully");
        res.send("Email sent successfully");
      }
    });
  } catch (error) {
    res.status(500).send("error in sending email" + error);
  }
};
module.exports = {
  email,
};
