var { testRoute1 } = require("./src/route/test_route.js");
var { emailRoute } = require("./src/route/emailSender_route.js");

var mainRoute = [
    ...testRoute1,
    ...emailRoute
];

module.exports = { mainRoute };
