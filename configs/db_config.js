var sequelize = require("sequelize");

const seq = new sequelize('nodetest', 'root','',{
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
    acquire: 30000,
  },
  logging: false,
});

//seq.sync({ alter:true, logging: true });

module.exports = seq;
