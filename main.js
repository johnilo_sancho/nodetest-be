const express = require("express");
const app = express();
const test = require("./src/controller/test_cont.js");
const {mainRoute} = require("./route.js");
const cors = require("cors");
const {checkToken} = require("./src/middleware/auth.js");

// app.post("/function", function (req, res) {
//   // res.send("Hello World");
//   test(req, res);
//   //res.json({ value: "the value" });
// });

//test added
//test added 2

var corsOpt = {
  methods: "GET,POST",
  allowedHeaders: ["Content-Type"],
  optionsSuccessStatus: 204,
};

app.use(cors(corsOpt));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const noAuth = [
"/login",
"/signup",
//"/fetch",
"/email"
];

mainRoute.forEach((item) => {
  if (!noAuth.includes(item.path)) {
    app[item.method](item.path, checkToken, item.action);
  } else {
    app[item.method](item.path, item.action);
  }
});

var port = 3000;
app.listen(port);
console.log("App running on port " + port);
